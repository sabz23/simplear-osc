using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Com.DeepScopeVR.OSC;

public class SettingsHandler : MonoBehaviour
{

    public OSC osc;

    public TextMeshProUGUI ip;
    public TextMeshProUGUI inPort;
    public TextMeshProUGUI outPort;

    public TMP_InputField ipInput;

    private string[] ipParts;


    // Start is called before the first frame update
    void Awake()
    {
        if (osc == null)
            osc = GameObject.Find("OSC").GetComponent<OSC>();
    }

    private void OnEnable()
    {
        //Debug.Log(GameManager.GetLocalIPAddress());
        string localIP = PlayerPrefs.GetString("deepscopeIP", GameManager.GetLocalIPAddress());
        ipParts = localIP.Split(".");
        ip.text = osc.outIP;
        inPort.text = osc.inPort.ToString();
        outPort.text = osc.outPort.ToString();
        ipInput.text = ipParts[3];
    }

    public void OnSaveButtonClick() {
        Debug.Log(GetModifiedIP());
        PlayerPrefs.SetString("deepscopeIP", GetModifiedIP());
        Application.Quit();
    }

    private string GetModifiedIP() {
        return ipParts[0] + "." + ipParts[1] + "." + ipParts[2] + "." + ipInput.text;
    }

}
