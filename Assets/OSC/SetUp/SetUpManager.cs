using System.Collections;
using System.Collections.Generic;
using Com.DeepScopeVR.OSC;
using UnityEngine;

public class SetUpManager : MonoBehaviour
{
    #region PublicProperties
    public GameObject[] screens;  //reference to the screens
    #endregion

    #region PrivateProperties
    private GameObject currentScreen;
    #endregion

    #region UnityCallbacks
    // Start is called before the first frame update
    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        SwitchScreen(GameState.Home);
    }
    #endregion

    #region ButtonClicks
    public void OnSetUpButtonClick() {
        //start the setup process
        InitiateSetUp();
    }

    public void OnTrackButtonClick() {
        SwitchScreen(GameState.ARTracking);
    }

    public void OnTrackCancelButtonClick() {
        SwitchScreen(GameState.Home);
    }

    public void OnSettingsButtonClick() {
        SwitchScreen(GameState.Settings);
    }

    public void OnSettingsCloseButtonClick()
    {
        SwitchScreen(GameState.Home);
    }
    #endregion

    #region PublicMembers
    public void OnSetUpCompleted() {
        //setup either compltetd or cancelled
        //setupScreen.SetActive(false);
        SwitchScreen(GameState.ARTracking);
    }
    #endregion

    #region HelperMembers
    private void InitiateSetUp() {
        //show setup screen
        SwitchScreen(GameState.SetUp);
    }

    public void SwitchScreen(GameState gameState)
    {
        int index = (int)gameState;
        if (currentScreen)
            currentScreen.SetActive(false);
        GameManager.Instance.CurrentGameState = gameState;
        currentScreen = screens[index];
        if (currentScreen)
            currentScreen.SetActive(true);
    }
    #endregion

}
