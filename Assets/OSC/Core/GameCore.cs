using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Com.DeepScopeVR.OSC {

    public enum GameState { Home, SetUp, ARTracking, Settings}

    public class GameManager {
        private static GameManager gameManagerInstance; //signleton instance

        public GameState CurrentGameState { get; set; } //current state of the game

        public static GameManager Instance
        {
            get
            {
                if (gameManagerInstance == null)
                    gameManagerInstance = new GameManager();
                return gameManagerInstance;
            }
        }

        private GameManager() {
            CurrentGameState = GameState.Home;
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }

}