using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendData : MonoBehaviour
{
    public SetUpManager setUpManagerScript;
    public OSC osc;
    public Transform arCameraTransform;

    // Start is called before the first frame update
    void Start()
    {
        if (osc == null)
            osc = GameObject.Find("OSC").GetComponent<OSC>();
        Debug.Log("Should send data");
    }

    public void OnConfirm() {
        OscMessage message = new OscMessage();

        message.address = "/ConfirmXYZ";
        message.values.Add(arCameraTransform.position.x * osc.moveDelta);
        message.values.Add(arCameraTransform.position.y * osc.moveDelta);
        message.values.Add(arCameraTransform.position.z * osc.moveDelta);


        message.values.Add(arCameraTransform.localRotation.eulerAngles.x);
        message.values.Add(arCameraTransform.localRotation.eulerAngles.y);
        message.values.Add(arCameraTransform.localRotation.eulerAngles.z);
        osc.Send(message);
        Debug.Log("ConfirmXYZ");

        //setUpManagerScript.OnSetUpCompleted();
    }

    public void OnCancel() {
        setUpManagerScript.OnSetUpCompleted();
    }


}
