﻿using UnityEngine;
//using System.Collections;
//using UnityEngine.XR.ARFoundation;
using Com.DeepScopeVR.OSC;
using System;
//using UnityEngine.InputSystem.XR;

public class SendPositionOnUpdate : MonoBehaviour {

	public OSC osc;
    //public TrackedPoseDriver tracked;


	// Use this for initialization
	void Start () {
        if(osc==null)
            osc = GameObject.Find("OSC").GetComponent<OSC>();
        Debug.Log("Should send data");
        //Debug.Log("IS URT - " + tracked.UseRelativeTransform);
        Vector3 pos = transform.localPosition;
        pos.y -= float.Parse(PlayerPrefs.GetString("deepscopeHeight", "0.0158"));
        transform.localPosition = pos;
    }
	
	// Update is called once per frame
	void Update () {

        if (GameManager.Instance.CurrentGameState != GameState.ARTracking)
            return;

        OscMessage message = new OscMessage();

        message.address = "/UpdateXYZ";
        message.values.Add(transform.position.x * osc.moveDelta);
        message.values.Add(transform.position.y * osc.moveDelta);
        message.values.Add(transform.position.z * osc.moveDelta);


        message.values.Add(transform.rotation.eulerAngles.x);
        message.values.Add(transform.rotation.eulerAngles.y);
        message.values.Add(transform.rotation.eulerAngles.z);
        osc.Send(message);
        Debug.Log("UpdateXYZ");

        //message = new OscMessage();
        //message.address = "/UpdateX";
        //message.values.Add(transform.position.x);
        //osc.Send(message);

        //message = new OscMessage();
        //message.address = "/UpdateY";
        //message.values.Add(transform.position.y);
        //osc.Send(message);

        //message = new OscMessage();
        //message.address = "/UpdateZ";
        //message.values.Add(transform.position.z);
        //osc.Send(message);


    }


}
